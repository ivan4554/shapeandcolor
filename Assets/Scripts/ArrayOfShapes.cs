﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Этот класс отвечает за хранение данных цвета и спрайта
public class ArrayOfShapes : MonoBehaviour
{
    public List<Sprite> AllSprites { get;} = new List<Sprite>();
    public List<Color> AllColors { get;} = new List<Color>();

    public void AddElement(SpriteRenderer main_sprite)
    {
        AllSprites.Add(main_sprite.sprite);
        AllColors.Add(main_sprite.color);
    }
    public void RemoveElement()
    {
        AllSprites.RemoveAt(0);
        AllColors.RemoveAt(0);
    }
    public bool IsSprite(int level, SpriteRenderer main_sprite)
    {
        level += 1;
        if (AllSprites.Count < level)
            return false;
        return AllSprites[AllSprites.Count - level] == main_sprite.sprite;
    }
    public bool IsColor(int level, SpriteRenderer main_sprite)
    {
        level += 1;
        if (AllColors.Count < level)
            return false;
        return AllColors[AllColors.Count - level] == main_sprite.color;
    }
}
