﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace
{
    public class FireBaseNextScene : MonoBehaviour
    {
        private const string SceneName = "ShapeAndColor";

        private IEnumerator Start()
        {
            while (FirebaseManager.Instance == null)
            {
                yield return new WaitUntil(() => FirebaseManager.Instance != null);
            }

            NextScene(SceneName);
        }

        private void NextScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}