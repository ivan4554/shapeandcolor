﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] int level = 0;
    public int Level { get { return level; } set { level = value; } }
    public void Next() 
    {
        level++;
        Debug.Log("Вы перешли на новый уровень!");
    }
}
