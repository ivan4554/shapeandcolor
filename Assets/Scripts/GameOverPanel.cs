﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class GameOverPanel : MonoBehaviour
    {
        private const string ScoreProgress = "ScoreProgress";
        public ScoreController ScoreController;
        
        [SerializeField] private Text AmountRoundsText;
        [SerializeField] private Text AmountPointsText;
        private int _amountRounds;


        private void Update()
        {
            if (gameObject.activeSelf) 
                PlayerPrefs.SetInt(ScoreProgress, GetPoints(ScoreController.Points));
        }

        public void GetRound(int round)
        {
            _amountRounds += round;
            AmountRoundsText.text = $"Пройдено раундов: {_amountRounds.ToString()}";
        }

        public void StartOver() => 
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        private int GetPoints(int points)
        {
            AmountPointsText.text = $"Получено очков: {points.ToString()}";
            return points;
        }


        public void QuitOver() => 
            Application.Quit();
    }
}