﻿using System;
using System.Collections;
using FullSerializer;
using Proyecto26;
using RSG;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

namespace Infastructure
{
    public class GameBootstrapper : MonoBehaviour
    {
        private const string LevelProgress = "LevelProgress";
        private const string ScoreProgress = "ScoreProgress";
        public GameController GameController;
        public TimeController TimeController;
        public ScoreController ScoreController;
        [Header("Active Level")]
        public Level ActiveLevel;
        

        private static readonly fsSerializer serializer = new fsSerializer();
        //static readonly string _jsonPath = $"{Application.streamingAssetsPath}/Levels.json";
        //private const string FirebaseUrl = "https://shapeandcolor-8e05c-default-rtdb.europe-west1.firebasedatabase.app/.json";
        private string jsonPost;
        private string _jsonData;
        private IPromise<ResponseHelper> _promise;
        private Levels _levels;

        private void Awake()
        {
            GameController.Level = PlayerPrefs.GetInt(LevelProgress);
            ScoreController.Points = PlayerPrefs.GetInt(ScoreProgress);
            LoadData(OnLoaded);
        }

        private void OnLoaded() =>
            AssignNewLevel(OnLoadedTimer);

        public void AssignNewLevel(Action onLoadedTimer = null)
        {
            ActiveLevel = GetLevel(_levels);
            ScoreController.MinPointsToWin = ActiveLevel.minPointsToWin;
            onLoadedTimer?.Invoke();
        }

        private void OnLoadedTimer() => 
            TimeController.Timer = ActiveLevel.time;

        private void LoadData(Action onLoaded = null)
        {
            FirebaseManager.Instance.Database.GetObject<Levels>("", (data) =>
            {
                Debug.Log($"Data: {data}");
                _levels = data;
                onLoaded.Invoke();
            }, (exception) =>
            {
                Debug.LogError(exception.Message);
            });
        }
            
            private Level GetLevel(Levels levels)
            {
                Level level = levels.levelsDic[$"level{GameController.Level}"];
                PlayerPrefs.SetInt(LevelProgress,GameController.Level);
                return level;
            }

    }
}
