﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Level
{
    public int minPointsToWin;
    public int correctAnswerPoints;
    public float time;
    public int spritesCount;
    public int stepBack;

    public Level(int minPointsToWin, int correctAnswerPoints, float time, int spritesCount, int stepBack)
    {
        this.minPointsToWin = minPointsToWin;
        this.correctAnswerPoints = correctAnswerPoints;
        this.time = time;
        this.spritesCount = spritesCount;
        this.stepBack = stepBack;
    }
}
