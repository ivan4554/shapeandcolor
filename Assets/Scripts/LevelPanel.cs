﻿using System.Collections;
using System.Collections.Generic;
using Infastructure;
using UnityEngine;
using UnityEngine.UI;
public class LevelPanel : MonoBehaviour
{
    [SerializeField] private GameBootstrapper GameBootstrapper;
    [SerializeField] GameController controller;
    [SerializeField] private Text showLevelText;
    [SerializeField] private Text matchLevelText;
    [SerializeField] private Text textHeadingLevel;
    [SerializeField] float delayLevelPanel; 
    [SerializeField] ColorScript mainColor;
    private void Start()
    {
        showLevelText.text = string.Empty;
    }
    public void TextOutput() 
    {
        // todo Refactor
        matchLevelText.text = $"Соответствует << {GameBootstrapper.ActiveLevel.stepBack}";
        showLevelText.text = $"{controller.Level + 1} УРОВЕНЬ!";
        textHeadingLevel.text = $"{controller.Level + 1}/8";
    }
    public void LevelNextMethod() 
    {
        controller.Next();
        Debug.Log("Вы перешли на новый уровень!");
    }

    public void ShowLevelPanel() =>
        StartCoroutine(ShowLevelPanelCoroutine(0, 140));
    private IEnumerator ShowLevelPanelCoroutine(float startSize,float endSize) 
    {
        Color startColor = Color.black;
        Color endColor = Color.clear;
        showLevelText.color = mainColor.GetColor();

        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime;
            GetComponent<Image>().color = Color.Lerp(startColor,endColor,t);
            showLevelText.fontSize = (int)Mathf.Lerp(startSize,endSize,t);
            yield return new WaitForSeconds(delayLevelPanel);
        }
        showLevelText.fontSize = (int)startSize;
        showLevelText.text = string.Empty;
        Debug.Log("Новый Level");
    }
}
