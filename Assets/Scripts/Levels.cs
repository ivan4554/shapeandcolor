﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Levels
{
    public Dictionary<string, Level> levelsDic = new Dictionary<string, Level>();
}
