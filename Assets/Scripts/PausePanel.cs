﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace
{
    public class PausePanel : MonoBehaviour
    {
        public void ContinueOver()
        {
            Time.timeScale = 1;
            gameObject.SetActive(false);       
        }

        public void StartOver()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void QuitOver()
        {
            Time.timeScale = 1;
            Application.Quit();
        }
    }
}