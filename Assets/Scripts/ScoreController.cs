﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    [SerializeField]private Text textScore;
    [HideInInspector]public int MinPointsToWin;
    public int Points { get; set; }

    private void Start()
    {
        textScore.text = $"{Points}";
    }

    private void Update()
    {
        if(IsPointsToWin())
            Debug.Log("Победа по очкам");
    }

    public bool IsPointsToWin()
    {
        return Points >= MinPointsToWin;
    }


    public void AddScore(int amount) 
    {
        Points += amount;
        Debug.Log($"Добавлено {amount} очков");
        textScore.text = $"{Points}";
    }
    public void SubtractScore(int amount) 
    {
        Points -= amount;
        Points = Mathf.Clamp(Points,0,1000);
        textScore.text = $"{Points}";
    }

    public void CleaningPoints()
    {
        Points = 0;
        textScore.text = Points.ToString();
    }
}
