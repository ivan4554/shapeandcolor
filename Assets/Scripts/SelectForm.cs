﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using BestHTTP.JSON;
using DefaultNamespace;
using Infastructure;
using UnityEngine;
using UnityEngine.Networking.Types;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SelectForm : MonoBehaviour
{
    [Header("Services")] [SerializeField] private GameBootstrapper GameBootstrapper;
    [SerializeField] private GameOverPanel GameOver;
    [SerializeField] private ArrayOfShapes ArrayOfShapes;
    [SerializeField] private LevelPanel LevelPanel;
    [SerializeField] private ScoreController ScoreController;
    [SerializeField] private TimeController TimeController;
    [SerializeField] private GameController GameController;
    [SerializeField] private MessagePanel MessagePanel;
    [SerializeField] private PausePanel PausePanel;
    
    [Header("Other")]
    [SerializeField] private SpriteRenderer main_sprite;
    [SerializeField] private GameObject mainComponents;
    [SerializeField] private Button startPlayBtn;
    
    [Header("Data")]
    public DataFigure[] dataFigures;
    
    private DataFigure spriteData;

    private bool isActive = false;
    private Animator main_sprite_anim;
    ColorScript _mainColor;

    private const int maxLevel = 10;
    private void Awake()
    {
        Time.timeScale = 0;
        main_sprite_anim = main_sprite.GetComponent<Animator>();
        main_sprite_anim.enabled = false;
        _mainColor = GetComponent<ColorScript>();
        mainComponents.SetActive(false);
        GameOver.gameObject.SetActive(false);
        PausePanel.gameObject.SetActive(false);
        startPlayBtn.onClick.AddListener(StartPlay);
    }

    public void SelectBtn(bool isEven) 
    {
        isActive = !isActive;
        if ((TypeFormOrColor() && isEven) || (!TypeFormOrColor() && !isEven))
        {
            ScoreController.AddScore(GameBootstrapper.ActiveLevel.correctAnswerPoints);
            Won();
        }
        else 
        {
            ScoreController.SubtractScore(GameBootstrapper.ActiveLevel.correctAnswerPoints);
            Debug.Log("Вы не угадали");
            Repeat();
        }
    }

    public void PauseBtn()
    {
        PausePanel.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    private void StartPlay()
    {
        Time.timeScale = 1;
        mainComponents.SetActive(true);
        main_sprite_anim.enabled = true;
        Initialize();
        startPlayBtn.gameObject.SetActive(false);
    }

    private void SetNewSprite(SpriteRenderer sprite) 
    {
        sprite.sprite = RandomSprites(GetSprites());
        sprite.color = _mainColor.RandomColor();
        ArrayOfShapes.AddElement(main_sprite);
    }

    private void Initialize()
    {
        LevelPanel.TextOutput();
        LevelPanel.ShowLevelPanel();
        ArrayOfShapes.AddElement(main_sprite);
        SetDataFigure();
        ChangeCard();
    }

    private void ChangeCard()
        => StartCoroutine(ChangeCardCoroutine());

    private IEnumerator ChangeCardCoroutine()
    {
        CardAnim(true, false);

        yield return new WaitForSeconds(1.5f);

        SetNewSprite(main_sprite);

        CardAnim(false, true);
    }

    private void Won() 
    {
        Debug.Log("Вы угадали");
        RandomTypeFigure();
        MessagePanel.SelectMessage();
        if(ArrayOfShapes.AllSprites.Count > maxLevel || ArrayOfShapes.AllColors.Count > maxLevel)
            ArrayOfShapes.RemoveElement();
        if (ScoreController.IsPointsToWin()) 
        {
            GameOver.GetRound(1);
            Debug.Log("Переход на новый уровень");
            LevelPanel.LevelNextMethod();
            NewLevel();
            SetDataFigure();
        }
        ChangeCard();
    }

    private bool TypeFormOrColor() 
    {
        switch (MessagePanel.TypeFigure)
        {
            case TaskType.Form:
                Debug.Log($"stepBack: {GameBootstrapper.ActiveLevel.stepBack}");
                return ArrayOfShapes.IsSprite(GameBootstrapper.ActiveLevel.stepBack, main_sprite);
            case TaskType.Color:
                return ArrayOfShapes.IsColor(GameBootstrapper.ActiveLevel.stepBack,main_sprite);
        }
        return true;
    }

    private void RandomTypeFigure() => 
        MessagePanel.TypeFigure = (TaskType)Random.Range(0,2);

    private void SetDataFigure() => 
        spriteData = dataFigures[GameController.Level];

    private Sprite RandomSprites(Sprite[] sprites) 
    {
        int randSprite = Random.Range(0,sprites.Length);
        return sprites[randSprite];
    }

    private void CardAnim(bool isHide, bool isShow)
    {
        main_sprite_anim.SetBool("Hide", isHide);
        main_sprite_anim.SetBool("Show", isShow);
    }

    private void Repeat() 
    {
        Debug.Log("Повторите");
    }

    private Sprite[] GetSprites() 
    {
        return spriteData.sprites;
    }

    private void NewLevel()
    {
        GameBootstrapper.AssignNewLevel();
        LevelPanel.TextOutput();
        LevelPanel.ShowLevelPanel();
        Debug.Log("ActiveLevel.time" + GameBootstrapper.ActiveLevel.time);
    }
}
