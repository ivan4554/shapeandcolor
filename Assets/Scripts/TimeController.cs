﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{
    [SerializeField] private GameOverPanel GameOver;
    [SerializeField]private Text _countingTime;
    [HideInInspector]public float Timer;

    private void Update()
    {
        if (Timer >= 0)
        {
            _countingTime.text = $"{(int)Timer}";
            Timer -= Time.deltaTime;
        }
        else 
        {
            GameOver.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
